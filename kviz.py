import operator
from operator import itemgetter

def ucitaj_pitanja():
    pitanja = []
    with open("pitanjaodgovori.txt", "r") as f:
        pitanja = f.readlines()
    pitanja2 = {}
    for line in pitanja:
        podatak = line.strip()
        pitanje, odgovor = podatak.split(",")
        pitanja2[pitanje.strip()] = odgovor.strip()
    return pitanja2

def pokreni_kviz():
    br_pitanja = 0
    br_odgovora = 0
    print "Kviz se pokrece..."
    print "Unesite vase ime i prezime"
    ime = raw_input("upisite ime: ")
    prezime = raw_input("upisite prezime: ")
    pitanja = ucitaj_pitanja()
    for pitanje in pitanja:
        print "\n" + str(pitanje)
        print "a) Tocno\tb) Netocno"
        while True:
            odgovor = raw_input("Vas odgovor je:  ")
            if odgovor.lower() == "a":
                if pitanja[pitanje] == "tocno":
                    print "Tocan odgovor!!!"
                    br_odgovora += 1
                    break
                else:
                    print "Netocan odgovor"
                    break
            elif odgovor.lower() == "b":
                if pitanja[pitanje] == "netocno":
                    print "Tocan odgovor!!!"
                    br_odgovora += 1
                    break
                else:
                    print "Netocan odgovor"
                    break
            else:
                print "Krivi unos"
        br_pitanja += 1
    print ("\n")
    print "Cestitamo " + ime + " " + prezime + ",vas rezultat je: " + str(br_odgovora) + "/" + str(br_pitanja)
    score = 100 * br_odgovora / br_pitanja
    text_file = open("highscore.txt", "a")
    text_file.write(ime + " " + prezime + " ima rezultat: " + str(br_odgovora) + "/" + str(br_pitanja) + ";" + str(score))
    text_file.write("\n")

    text_file.close()
def hall_of_fame():
    players = []
    players2 = []
    with open ("highscore.txt", "r") as f:
        igraci = f.readlines()
        for line in igraci:
            igrac = line.strip()
            ispis, bodovi = igrac.split(";")
            players2.append([ispis.strip(),bodovi.strip()])
    players2_sorted = sorted(players2, key=lambda x: int(x[1]),reverse=True)
    for player in players2_sorted[:10]:
        print player[0]
def dodaj_pitanje():
    pitanje = raw_input("Unesi pitanje: ")
    while True:
        odgovor = raw_input("Unesite tocno ili netocno: ")
        if (odgovor == "tocno") or (odgovor == "netocno"):
            break
        else:
            print "Krivi unos"
    with open("pitanjaodgovori.txt", "a") as f:
        f.write(str(pitanje)+","+str(odgovor))
        f.write("\n")

while True:
    print "1. Pokreni kviz"
    print "2. Dodaj pitanje"
    print "3. Ispisi 10 najboljih rezultata"
    print "0. Prekid programa"
    opcija = raw_input("Odaberi opciju: ")
    if opcija == "1":
        pokreni_kviz()
    elif opcija == "2":
        dodaj_pitanje()
    elif opcija == "3":
        hall_of_fame()
    elif opcija == "0":
        print "Kviz se prekida"
        break
    else:
        print "Krivi unos"






